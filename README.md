This module changes fields in various ways.

It can resize, in an update function:

\Drupal::service('druidfire')->resize('paragraph', 'section_title', ['size' => 1024]);

Or 

drush druidfire resize paragraph section_title 1024

If you want to resize something else than the main property:

\Drupal::service('druidfire')->resize('paragraph', 'link', ['size' => 1024, 'property' => 'title']);

You can convert a non-formatted field to a formatted one:

\Drupal::service('druidfire')->string2formatted('paragraph', 'component_flora_answer');

An entity revision reference field to entity revision with err2er, to bricks with err2bricks:

\Drupal::service('druidfire')->err2er('paragraph', 'component_lee_referenced_item');

\Drupal::service('druidfire')->err2bricks('paragraph', 'paragraph_unlimited');

A string field to taxonomy reference with string2taxonomyReference:

\Drupal::service('druidfire')->string2taxonomyReference('node', 'time', ['vid' => 'event_time']);
